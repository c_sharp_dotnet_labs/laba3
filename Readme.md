# Задание

![t1](Task/t1.JPG)
![t2](Task/t2.JPG)
![t3](Task/t3.JPG)
![t4](Task/t4.JPG)

# Скриншоты

Вид главного окна:

![main Window](Screenshots/MainWindow.png)

Окно редактирования параметров компьютер (которое появляется при двойном клике по статус бару):

![](Screenshots/ComputerParametersEdit.JPG)

Другие окна:

![](Screenshots/AddProgramForm.JPG)

![](Screenshots/AddUserForm.JPG)


# Исполняемые файлы

Сборку для виндовс можно скачать [здесь](https://www.dropbox.com/s/g6lbe5ttnmlzo6r/ProgramManager.zip?dl=0).