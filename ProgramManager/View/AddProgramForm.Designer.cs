﻿namespace ProgramManager.View
{
    partial class AddProgramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDeveloper = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.nudVer1 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nudVer2 = new System.Windows.Forms.NumericUpDown();
            this.nudVer3 = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nudRAM = new System.Windows.Forms.NumericUpDown();
            this.nudCPU = new System.Windows.Forms.NumericUpDown();
            this.nudHDD = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudVer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVer3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHDD)).BeginInit();
            this.SuspendLayout();
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(167, 12);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(183, 27);
            this.lbName.TabIndex = 0;
            this.lbName.Text = "Имя программы:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(360, 5);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(257, 34);
            this.tbName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Разработчик:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbDeveloper
            // 
            this.tbDeveloper.Location = new System.Drawing.Point(360, 52);
            this.tbDeveloper.Name = "tbDeveloper";
            this.tbDeveloper.Size = new System.Drawing.Size(257, 34);
            this.tbDeveloper.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 27);
            this.label2.TabIndex = 4;
            this.label2.Text = "Версия:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(137, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(213, 27);
            this.label3.TabIndex = 6;
            this.label3.Text = "Необходимая RAM:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(74, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(276, 27);
            this.label4.TabIndex = 11;
            this.label4.Text = "Необходимая частота ЦП:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 259);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(344, 27);
            this.label6.TabIndex = 13;
            this.label6.Text = "Занимаемое пространство HDD:";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(479, 312);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(138, 55);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(348, 312);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 55);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // nudVer1
            // 
            this.nudVer1.Location = new System.Drawing.Point(360, 100);
            this.nudVer1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudVer1.Name = "nudVer1";
            this.nudVer1.Size = new System.Drawing.Size(67, 34);
            this.nudVer1.TabIndex = 16;
            this.nudVer1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(432, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 27);
            this.label5.TabIndex = 17;
            this.label5.Text = ".";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(533, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 27);
            this.label7.TabIndex = 18;
            this.label7.Text = ".";
            // 
            // nudVer2
            // 
            this.nudVer2.Location = new System.Drawing.Point(456, 102);
            this.nudVer2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudVer2.Name = "nudVer2";
            this.nudVer2.Size = new System.Drawing.Size(67, 34);
            this.nudVer2.TabIndex = 19;
            // 
            // nudVer3
            // 
            this.nudVer3.Location = new System.Drawing.Point(550, 102);
            this.nudVer3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudVer3.Name = "nudVer3";
            this.nudVer3.Size = new System.Drawing.Size(67, 34);
            this.nudVer3.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(557, 154);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 27);
            this.label8.TabIndex = 21;
            this.label8.Text = "MB";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(557, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 34);
            this.label9.TabIndex = 22;
            this.label9.Text = "MHz";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(557, 262);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 34);
            this.label10.TabIndex = 23;
            this.label10.Text = "GB";
            // 
            // nudRAM
            // 
            this.nudRAM.Location = new System.Drawing.Point(360, 151);
            this.nudRAM.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudRAM.Name = "nudRAM";
            this.nudRAM.Size = new System.Drawing.Size(191, 34);
            this.nudRAM.TabIndex = 24;
            // 
            // nudCPU
            // 
            this.nudCPU.Location = new System.Drawing.Point(360, 198);
            this.nudCPU.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudCPU.Name = "nudCPU";
            this.nudCPU.Size = new System.Drawing.Size(191, 34);
            this.nudCPU.TabIndex = 25;
            // 
            // nudHDD
            // 
            this.nudHDD.Location = new System.Drawing.Point(360, 252);
            this.nudHDD.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudHDD.Name = "nudHDD";
            this.nudHDD.Size = new System.Drawing.Size(191, 34);
            this.nudHDD.TabIndex = 26;
            // 
            // AddProgramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 383);
            this.Controls.Add(this.nudHDD);
            this.Controls.Add(this.nudCPU);
            this.Controls.Add(this.nudRAM);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.nudVer3);
            this.Controls.Add(this.nudVer2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudVer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDeveloper);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbName);
            this.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "AddProgramForm";
            this.Text = "AddProgramForm";
            ((System.ComponentModel.ISupportInitialize)(this.nudVer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVer3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHDD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDeveloper;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown nudVer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudVer2;
        private System.Windows.Forms.NumericUpDown nudVer3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudRAM;
        private System.Windows.Forms.NumericUpDown nudCPU;
        private System.Windows.Forms.NumericUpDown nudHDD;
    }
}