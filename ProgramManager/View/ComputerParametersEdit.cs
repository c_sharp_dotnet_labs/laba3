﻿using System;
using System.Windows.Forms;
using info = ProgramManager.Model.ProgramManager;

namespace ProgramManager.View
{
    public partial class ComputerParametersEdit : Form
    {
        public ComputerParametersEdit()
        {
            InitializeComponent();

            nudCPU.Value = info.CPU / info.MEGA;
            nudRAM.Value = info.RAM / info.MEGA;
            nudHDD.Value = info.HDD / 1000;
            tbOS.Text = info.OS;

            btnCancel.Click += BtnCancel_Click;
            btnChange.Click += BtnChange_Click;
        }

        private void BtnChange_Click(object sender, System.EventArgs e)
        {
            info.CPU = Convert.ToUInt64(nudCPU.Value) * info.MEGA;
            info.RAM = Convert.ToUInt64(nudRAM.Value) * info.MEGA;
            info.HDD = Convert.ToUInt64(nudHDD.Value) * 1000;
            info.OS = tbOS.Text;
            DialogResult = DialogResult.OK;
        }

        private void BtnCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
