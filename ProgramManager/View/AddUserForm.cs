﻿using System.Drawing;
using System.Windows.Forms;

namespace ProgramManager.View
{
    public partial class AddUserForm : Form
    {
        public AddUserForm()
        {
            InitializeComponent();

            btnAdd.Click += BtnAdd_Click;
            btnCancel.Click += BtnCancel_Click;

            tbPass1.PasswordChar = '*';
            tbPass2.PasswordChar = '*';
        }

        public Model.User resultUser { get; private set; }
        public void ChangeMode(string userName)
        {
            Model.User user = Model.ProgramManager.Users[userName];
            tbName.Text = user.Name;
            string pass = user.Password;
            if (null != pass)
            {
                tbPass1.Text = pass;
                tbPass2.Text = pass;
            }
            btnAdd.Text = "Изменить";
        }

        private void BtnCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtnAdd_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(tbName.Text))
            {
                MessageBox.Show(this, "Имя пусто", "Ошибка",
    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            } else if (tbPass1.Text != tbPass2.Text)
            {
                MessageBox.Show(this, "Пароли не совпадают", "Ошибка",
    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;

            } else
            {
                resultUser = new Model.User(tbName.Text, tbPass1.Text);
                DialogResult = DialogResult.OK;
            }

        }
    }
}
