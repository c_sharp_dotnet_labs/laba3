﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramManager.View
{
    public partial class AddProgramForm : Form
    {
        public AddProgramForm()
        {
            InitializeComponent();
            btnAdd.Click += BtnAdd_Click;
            btnCancel.Click += BtnCancel_Click;
        }

        public Model.Program ResultProg { get; private set; }

        public void ChangeMode(int index)
        {
            var prog = Model.ProgramManager.InstalledPrograms[index];
            tbName.Text = prog.Name;
            tbDeveloper.Text = prog.Developer;
            nudVer1.Value = prog.Version.FirstNum;
            nudVer2.Value = prog.Version.SecondNum;
            nudVer3.Value = prog.Version.ThirdNum;
            nudCPU.Value = prog.RequiredCPU / Model.ProgramManager.MEGA;
            nudRAM.Value = prog.RequiredRAM / Model.ProgramManager.MEGA;
            nudHDD.Value = prog.RequiredSpace / 1000;
            btnAdd.Text = "Изменить";
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            string[] arr = new string[]
            {
                tbName.Text, tbDeveloper.Text
            };
            foreach (string s in arr)
            {
                if (string.IsNullOrEmpty(s))
                {
                    MessageBox.Show(this, "Есть пустые поля", "Ошибка",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            ResultProg = new Model.Program(arr[0],  arr[1], Model.ProgramManager.OS,
                DateTime.Now, new Model.Version_Type( 
                    Convert.ToByte(nudVer1.Value), Convert.ToByte(nudVer2.Value),
                    Convert.ToByte(nudVer3.Value)), 
               Convert.ToUInt64(nudRAM.Value) * Model.ProgramManager.MEGA, 
               Convert.ToUInt64(nudCPU.Value) * Model.ProgramManager.MEGA,
               Convert.ToUInt64(nudHDD.Value) * 1000
            );
            DialogResult = DialogResult.OK;
        }
    }
}
