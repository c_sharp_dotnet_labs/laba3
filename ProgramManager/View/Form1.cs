﻿using ProgramManager.View;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ProgramManager
{
    public partial class Form1 : Form
    {
        private BindingSource _bsPrograms; 
        
        public Form1()
        {
            InitializeComponent();

            _bsPrograms = new BindingSource();
            _bsPrograms.DataSource = Model.ProgramManager.InstalledPrograms;
            dgvPrograms.DataSource = _bsPrograms;
            
            foreach(KeyValuePair<string, Model.User> entry in Model.ProgramManager.Users)
            {
                lbUsers.Items.Add(entry.Key);
            }

            toolStripStatusLabel.Text
                = Model.ProgramManager.ComputerParameters();
            statusStrip.DoubleClick += StatusStrip_DoubleClick;

            btnInstall.Click += BtnInstall_Click;
            btnDeInstall.Click += BtnDeInstall_Click;
            btnUpdate.Click += BtnUpdate_Click;
            btnAddUser.Click += BtnAddUser_Click;
            btnDeleteUser.Click += BtnDeleteUser_Click;
            lbUsers.DoubleClick += LbUsers_DoubleClick;
            dgvPrograms.DoubleClick += DgvPrograms_DoubleClick;
        }

        private void DgvPrograms_DoubleClick(object sender, System.EventArgs e)
        {
            var dialog = new AddProgramForm();
            dialog.StartPosition = FormStartPosition.CenterParent;
            var rows = dgvPrograms.SelectedRows;
            int index = rows[0].Index;
            if (1 != rows.Count || index < 0)
            {
                return;
            }
            dialog.ChangeMode(index);
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                _bsPrograms.ResetBindings(true);
                Model.ProgramManager.InstalledPrograms.RemoveAt(index);
                Model.ProgramManager.InstallProgram(dialog.ResultProg);
            }
        }

        private void LbUsers_DoubleClick(object sender, System.EventArgs e)
        {
            var dialog = new AddUserForm();
            dialog.StartPosition = FormStartPosition.CenterParent;
            int index = lbUsers.SelectedIndex;
            if (-1 == index)
            {
                throw new System.IndexOutOfRangeException();
            }
            dialog.ChangeMode(lbUsers.SelectedItem.ToString());
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Model.ProgramManager.AddUser(dialog.resultUser);
                lbUsers.Items.RemoveAt(index);
                lbUsers.Items.Add(dialog.resultUser.Name);
                lbUsers.Refresh();
            }
        }

        private void BtnDeleteUser_Click(object sender, System.EventArgs e)
        {
            int index = lbUsers.SelectedIndex;
            if (-1 == index)
            {
                MessageBox.Show("Выберите пользователя, которого нужно удалить.",
    "Не выбрано", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Model.ProgramManager.DeleteUser((string) lbUsers.Items[index]);
            lbUsers.Items.RemoveAt(index);
        }

        private void BtnAddUser_Click(object sender, System.EventArgs e)
        {
            var dialog = new AddUserForm();
            dialog.StartPosition = FormStartPosition.CenterParent;
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                lbUsers.Items.Add(dialog.resultUser.Name);
                Model.ProgramManager.AddUser(dialog.resultUser);
            }
        }

        private void BtnUpdate_Click(object sender, System.EventArgs e)
        {
            var rows = dgvPrograms.SelectedRows;
            if (0 == rows.Count)
            {
                MessageBox.Show("Выберите программы, которые нужно обновить.",
                    "Не выбрано", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                for (int i = 0; i < rows.Count; ++i)
                {
                    Model.ProgramManager.Update(rows[i].Index);
                }
                _bsPrograms.ResetBindings(true);
            }
        }

        private void BtnDeInstall_Click(object sender, System.EventArgs e)
        {
            var rows = dgvPrograms.SelectedRows;
            if (0 == rows.Count)
            {
                MessageBox.Show("Выберите строки таблицы, которые нужно удалить.",
                    "Не выбрано", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult res = MessageBox.Show(this, "Вы уверены?", "Внимание!",
                    MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    Model.ProgramManager.DeInstallPrograms(rows);
                    _bsPrograms.ResetBindings(true);
                }
            }
        }

        private void BtnInstall_Click(object sender, System.EventArgs e)
        {
            var dialog = new AddProgramForm();
            dialog.StartPosition = FormStartPosition.CenterParent;
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Model.ProgramManager.InstallProgram(dialog.ResultProg);
                _bsPrograms.ResetBindings(true);
            }
        }

        private void StatusStrip_DoubleClick(object sender, System.EventArgs e)
        {
            var dialog = new ComputerParametersEdit();
            dialog.StartPosition = FormStartPosition.CenterParent;
            dialog.ShowDialog();
            if(DialogResult.OK == dialog.DialogResult)
            {
                toolStripStatusLabel.Text
                = Model.ProgramManager.ComputerParameters();
            }
        }
    }
}
