﻿using System.Collections.Generic;

namespace ProgramManager.Model
{
    public class User
    {
        public string Name { get; set; }

        public string Password { get; set; }

        public LinkedList<Program> programs { get; set; }

        public bool Authentificate(string name, string password)
        {
            return name == Name && password == Password;
        }

        public static bool operator ==(User a, User b)
        {
            return a.programs.Count == b.programs.Count;
        }

        public static bool operator !=(User a, User b)
        {
            return a.programs.Count != b.programs.Count;
        }
        public static bool operator <(User a, User b)
        {
            return a.programs.Count < b.programs.Count;
        }
        public static bool operator >(User a, User b)
        {
            return !(a.programs.Count < b.programs.Count);
        }

        public User(string name, string password)
        {
            Name = name; Password = password;
            programs = new LinkedList<Program>(ProgramManager.InstalledPrograms);
        }
    }
}
