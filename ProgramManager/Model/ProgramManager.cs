﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace ProgramManager.Model
{
    static class ProgramManager
    {
        public const int MEGA = 1_000_000;
        public const int GIGA = 1_000_000_000;
        public static List<Program> InstalledPrograms { get; set; }
        public static Dictionary<string, User> Users { get; set; }
        public static int ProgramsNumber { get => InstalledPrograms.Count; }
        public static int UsersNumber { get => Users.Count; }
        public static string OS { get; set; }
        public static ulong CPU { get; set; }
        public static ulong RAM { get; set; }
        public static ulong HDD { get; set; } // in megabytes

        static ProgramManager()
        {
            OS = "Windows 10 (64)";
            CPU = 2_000_000_000;
            RAM = 8_000_000_000;
            HDD = 1_256_000;

            InstalledPrograms = new List<Program>();
            var pr = new Program(
                "Microsoft Word", OS, "Microsoft",
                DateTime.Now, new Version_Type(1, 0, 0),
                1_000_000_000, 500_000_000, 70_000
            );
            InstalledPrograms.Add(pr);
            InstalledPrograms.Add(new Program(
                "Visual Studio", OS, "Microsoft",
                DateTime.Now, new Version_Type(2, 0, 1),
                2_000_000_000, 1_000_000_000, 1_500_000
                ));

            Users = new Dictionary<string, User>();
            Users["Богдан"] = new User("Богдан", "1234");
            Users["Виктор"] = new User("Виктор", "abcd");
        }

        static void Allow(User user, Program program)
        {
            user.programs.AddLast(program);
        }

        static void DisAllow(User user, Program program)
        {
            user.programs.Remove(program);
        }

        static int Compare(Program a, Program b)
        {
            if (a < b)
            {
                return -1;
            } else if (a > b)
            {
                return 1;
            } else
            {
                return 0;
            }
        }

        static int Compare(User a, User b)
        {
            if (a == b)
            {
                return 0;
            }
            else if (a < b)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }

        public static void Update(int index)
        {
            Program program = InstalledPrograms[index];
            byte[] arr = new byte[Version_Type.N]
            {
                program.Version.FirstNum, program.Version.SecondNum,
                program.Version.ThirdNum
            };

            for (int i = Version_Type.N - 1; 0 <= i; --i)
            {
                if (arr[i] != 255)
                {
                    ++arr[i];
                    break;
                }
            }

            program.VersionUpdate(new Version_Type(arr[0], arr[1], arr[2]));
            program.RequiredCPU += 2000;
            program.RequiredRAM += 1000;
            program.RequiredSpace += 500;
        }

        public static void InstallProgram(Program pr)
        {
            if (null == pr)
            {
                throw new ArgumentException("pr == null");
            }
            else
            {
                InstalledPrograms.Add(pr);
            }
        }

        public static void DeInstallPrograms (DataGridViewSelectedRowCollection rows)
        {
            int min = rows[0].Index;
            for (int i = 1; i < rows.Count; ++i)
            {
                if (rows[i].Index < min)
                {
                    min = rows[i].Index;
                }
            }
            InstalledPrograms.RemoveRange(min, rows.Count);
        }

        public static void AddUser(User user)
        {
            if (string.IsNullOrEmpty(user.Name))
            {
                throw new ArgumentException("user.Name is null or empty");
            } else
            {
                Users[user.Name] = user;
            }
        }

        public static void DeleteUser(string key)
        {
            Users.Remove(key);
        }

        public static string ComputerParameters()
        {
            return $"Частота процессора: { CPU / (double) GIGA } GHz, " +
                $"Оперативная память: {RAM / (double) GIGA} ГБ, " +
                $"Дисковое простаннство: {HDD / (double) MEGA} ТБ, " +
                $"OC: {OS}";

        }
    }
}
