﻿using System;

namespace ProgramManager.Model
{
    public struct Version_Type {
        public byte FirstNum { get; set; } // first number
        public byte SecondNum { get; set; }  // second number
        public byte ThirdNum { get; set; }  //third number

        //specifies number of numbers used to define version
        public const byte N = 3;

        public Version_Type(byte firstNum, byte secondNum, byte thirdNum)
        {
            FirstNum = firstNum;
            SecondNum = secondNum;
            ThirdNum = thirdNum;
        }

        public override string ToString()
        {
            return $"{FirstNum},{SecondNum},{ThirdNum}";
        }
    }
    public class Program
    {
        private Version_Type _version;
        private string _name;
        public string Name { get => _name;
            set {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name can't be null or empty");
                } else
                {
                    _name = value;
                }
            } }
        public string OS { get; set; }
        public string Developer { get; set; }
        public DateTime InstallationDate { get; set; }
        public Version_Type Version { get => _version; } // to set Version use VersionUpdate()
        public ulong RequiredRAM { get; set; }
        public ulong RequiredCPU { get; set; }
        public ulong RequiredSpace { get; set; }

        public void VersionUpdate(Version_Type newVersion)
        {
            _version = newVersion;
            var exc = new ArgumentException("New version is lolder than current");
            
            // array of numbers of current version
            byte[] curr_ver = new byte[Version_Type.N] { _version.FirstNum, 
                _version.SecondNum, _version.ThirdNum };
            
            // array of numbers of new version
            byte[] new_ver = new byte[Version_Type.N] { newVersion.FirstNum,
                newVersion.SecondNum, newVersion.ThirdNum };

            for (int i = 0; i < Version_Type.N; ++i)
            {
                if (curr_ver[i] < new_ver[i])
                {
                    return;
                }
                else if (new_ver[i] < curr_ver[i])
                {
                    throw exc;
                }
            }
        }

        public Program(string name, string os, string devel, DateTime date,
            Version_Type version, ulong ram, ulong cpu, ulong hdd)
        {
            Name = name; OS = os; Developer = devel; InstallationDate = date;
            VersionUpdate(version);
            RequiredRAM = ram; RequiredCPU = cpu; RequiredSpace = hdd;
        }

        public static bool operator> (Program a, Program b)
        {
            return a.RequiredRAM + a.RequiredCPU + a.RequiredSpace 
                >
                b.RequiredRAM + b.RequiredCPU + b.RequiredSpace;
        }

        private static ulong _sum(Program pr)
        {
            return pr.RequiredRAM + pr.RequiredCPU + pr.RequiredSpace;
        }

        public static bool operator< (Program a, Program b)
        {
            return _sum(a) < _sum(b);
        }
    }
}
